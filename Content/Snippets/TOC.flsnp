﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="18" MadCap:lastHeight="1818" MadCap:lastWidth="807">
    <head>
    </head>
    <body>
        <nav id="docs-content">
            <h3>Contents</h3>
            <ul class="nav">
                <li>
                    <button data-toggle="collapse" data-target="#item-43">FAQs
                    </button>
                    <ul class="collapse" id="item-43">
                        <li>
                            <button data-toggle="collapse" data-target="#item-46">Installation</button>
                            <ul class="collapse" id="item-46">
                                <li><a href="../difference-between-a-clean-install-and-upgrade-of-zoomdata.html">What is the difference between a clean install and an upgrade of Zoomdata?</a>
                                </li>
                                <li><a href="../can-zoomdata-be-installed-from-an-rpm-repository.html">Can Zoomdata be installed from an RPM&#160;repository?</a>
                                </li>
                                <li>
                                    <button data-toggle="collapse" data-target="#item-47"><a href="../do-you-have-a-rpm-for-red-hat-linux.html">Do you have a RPM for Red Hat Linux?</a>
                                    </button>
                                </li>
                                <li><a href="../what-are-the-minimum-system-requirements-for-rpm-installations.html">What are the minimum system requirements for RPM installation?</a>
                                </li>
                                <li><a href="../what-java-version-does-zoomdata-support.html">What Java version does Zoomdata support?</a>
                                </li>
                                <li><a href="../how-do-i-configure-zoomdata-s-memory-settings.html">How do I configure Zoomdata's memory settings?</a>
                                </li>
                                <li><a href="../how-does-zoomdata-handle-certificate-based-authentication.html">How does Zoomdata handle certificate-based authentication? </a>
                                </li>
                                <li><a href="../faq-what-are-the-zoomdata-conf-and-zoomdata-env-files.html">What are the Zoomdata property files?</a>
                                </li>
                                <li><a href="../can-the-real-time-sales-demo-source-be-enabled.html">Can the Real Time Sales demo source be disabled?</a>
                                </li>
                                <li><a href="../why-can-i-not-configure-the-firewall-on-my-zoomdata-server-by-using-iptables-when-using-centos-7.html">Why can't I&#160;configure the firewall using iptables on CentOS7?</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <button data-toggle="collapse" data-target="#item-48">Performance</button>
                            <ul class="collapse" id="item-48">
                                <li><a href="../what-can-we-expect-in-term-of-response-time-with-reference-to-data-size.html">What can we expect in terms of response time with data size?</a>
                                </li>
                                <li><a href="../what-is-the-availability-of-the-zoomdata-application.html">What is the availability of the Zoomdata Application?</a>
                                </li>
                                <li><a href="../what-is-the-user-capacity-for-each-zoomdata-server.html">What is the user capacity for each Zoomdata Server?</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <button data-toggle="collapse" data-target="#item-49">Administration</button>
                            <ul class="collapse" id="item-49">
                                <li><a href="../how-do-i-reset-my-supervisor-password.html">How do I&#160;reset my Supervisor password?</a>
                                </li>
                                <li><a href="../how-to-enable-debug-mode-in-zoomdata.html">How do I&#160;enable DEBUG mode in Zoomdata?</a>
                                </li>
                                <li><a href="../how-can-i-check-if-zoomdata-restarted-properly.html">How can I check if Zoomdata has restarted properly?</a>
                                </li>
                                <li><a href="../is-zoomdata-able-to-connect-to-spark-on-mesos.html">Is Zoomdata able to connect to Spark on Mesos?</a>
                                </li>
                                <li><a href="../what-is-the-pidxxxx-hprof-file-on-my-zoomdata-server-and-why-is-it-being-generated.html">What is the pidXXXX.hprof file on my Zoomdata server?</a>
                                </li>
                                <li>How can I&#160;remove the Enterprise Data Connector Internal from the data sources home page?</li>
                                <li><a href="../how-can-i-manually-change-the-chart-dashboard-icons-if-screenshot-is-not-enabled.html">How can I&#160;upload custom images for my charts and dashboards?</a>
                                </li>
                                <li><a href="../how-can-i-configure-zoomdata-to-use-a-different-url-endpoint.html">Can Zoomdata be configured to use a different URL&#160;endpoint?</a>
                                    <br />
                                </li>
                            </ul>
                        </li>
                        <li>
                            <button data-toggle="collapse" data-target="#item-51">Data Sources</button>
                            <ul class="collapse" id="item-51">
                                <li><a href="../can-zoomdata-connect-to-spark-on-a-yarn-server.html">Can Zoomdata connect to Spark on a Yarn Server?</a>
                                </li>
                                <li>
                                    <button data-toggle="collapse" data-target="#item-52"><a href="../do-cloudera-impala-sources-require-the-timestamp-column-or-can-this-be-left-blank.html">Do Cloudera Impala sources require the timestamp column?</a>
                                    </button>
                                </li>
                                <li><a href="../does-cloudera-impala-sources-require-a-partitioned-scheme-declared.html">Do Cloudera Impala sources require a partitioned scheme declared?</a>
                                </li>
                                <li><a href="../for-elasticsearch-and-solr-connections-does-zoomdata-support-nested-documents.html">Does Zoomdata support Data with Nested Objects?</a>
                                </li>
                                <li><a href="../for-elasticsearch-connections-are-the-dashboards-in-zoomdata-resulting-from-a-single-query.html">For Elasticsearch connections, are the dashboards in Zoomdata resulting from a single query?</a>
                                </li>
                                <li><a href="../for-solr-connections-does-zoomdata-support-queries-to-multiple-indices-and-create-a-single-dashboard.html">For Solr connections, does Zoomdata support queries to multiple indices?</a> <![CDATA[ ]]></li>
                                <li><a href="../how-do-i-enable-distinct-counts-in-zoomdata.html">How do I enable Distinct Counts in Zoomdata?</a>
                                </li>
                                <li><a href="../impala-can-i-specify-the-principal-username-in-the-jdbc-url-when-connecting-to-a-secure-cdh-cluster.html">For Impala, can I specify the principal username in the JDBC&#160;URL when connecting to a secure CDH&#160;cluster?</a>
                                </li>
                                <li><a href="../what-date-and-time-formats-does-zoomdata-support.html">What Date and Time formats does Zoomdata support?</a>
                                </li>
                                <li><a href="../does-zoomdata-support-connecting-to-impala-over-ssl.html">Does Zoomdata support connecting to Impala over SSL?</a>
                                </li>
                                <li><a href="../why-is-the-principal-parameter-always-impala-specifically-in-the-jdbc-url-when-using-kerberized-impala.html">Why is the pricipal parameter always 'impala' in the JDBC URL when using kerberized Impala?</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <button data-toggle="collapse" data-target="#item-53">Visualizations and Dashboards</button>
                            <ul class="collapse" id="item-53">
                                <li><a href="../can-i-use-multiple-connections-on-the-same-visualization.html">Can I&#160;use multiple connections on the same visualization?</a>
                                </li>
                                <li><a href="../for-the-stacked-bars-style-are-labels-displayed-for-each-segment-within-a-stack.html">For the Stacked Bars Style, are labels displayed for each segment?</a>
                                </li>
                                <li><a href="../how-can-i-change-the-default-color-palette-used-by-my-visualizations.html">How can I change the default color palette for my visualizations?</a>
                                </li>
                                <li><a href="../is-a-legend-available-to-identify-the-colors-of-the-visualization.html">Is a color legend available to identify the colors of my visualizations?</a>
                                </li>
                                <li><span><a href="../is-it-possible-to-clone-or-copy-dashboards.html">Is it possible to clone or copy dashboards?</a></span>
                                </li>
                                <li><a href="../what-fields-are-required-to-show-data-in-zoomable-maps.html">What fields are required to show data in zoomable maps?</a>
                                </li>
                                <li><a href="../how-does-sort-limit-logic-work-in-visualizations-using-two-attribute-group-bys.html">How does Sort and Limit work in visualizations using two attribute group-bys?</a>
                                </li>
                                <li><a href="../how-can-i-disable-the-browser-warning-that-triggers-when-users-try-to-leave-a-dashboard-without-saving.html">How can I disable the 'unsaved dashboards' browser warning?</a>
                                </li>
                                <li><a href="../will-caching-work-if-i-am-using-a-rolling-time-filter-on-my-dashboard.html">Does caching work if I am using a rolling time filter on my dashboard?</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <button data-toggle="collapse" data-target="#item-54">Developer Resources</button>
                            <ul class="collapse" id="item-54">
                                <li><a href="../how-can-i-modify-the-look-of-the-input-boxes-in-zoomdata.html">How can I&#160;modify the look of the input boxes in Zoomdata?</a>
                                </li>
                                <li><a href="../how-can-i-modify-the-format-of-icons-such-as-rulers-icon-that-uses-an-image-instead-of-a-font.html">How can I&#160;modify the format of Icons that use images instead of fonts?</a>
                                </li>
                                <li><a href="../what’s-the-difference-between-the-zoomdata-visualization-api-and-the-zoomdata-javascript-sdk.html">What is the difference between Zoomdata Visualization API and Zoomdata Javascript SDK?</a>
                                </li>
                                <li><a href="../can-i-use-zoomdata-visualizations-on-my-own-webpage.html">Can I use Zoomdata visualizations on my own webpage?</a>
                                </li>
                                <li><a href="../is-it-possible-to-modify-an-existing-visualization-template.html">Is it possible to modify an existing visualization template?</a>
                                </li>
                                <li><a href="../can-i-use-one-visualization-to-set-the-filter-for-another-visualization.html">Can I&#160;use one visualization to set the filter for another visualization?</a>
                                </li>
                                <li><a href="../how-do-i-use-html-controls-to-set-filters-and-group-by-properties-for-visualizations-embedded-in-a-webpage.html">How do I&#160;use HTML controls to set filters and properties for embedded visualizations?</a>
                                </li>
                                <li><a href="../how-do-i-fix-resizing-errors-with-visualizations-embedded-in-my-own-webpage.html">How do I&#160;fix resizing errors with embedded visualizations?</a>
                                </li>
                                <li><a href="../where-are-the-drawing-functions-in-the-zoomdata-visualization-api.html">Where are the Drawing Functions in the Zoomdata Visualization API?</a>
                                </li>
                                <li><a href="../sdk-does-the-radial-popup-menu-show-on-my-sdk-embeds.html">Does the Radial Menu show on my SDK embeds?</a>
                                </li>
                                <li><a href="../where-is-the-location-of-the-custom-javascript-visualization-code-created-using-visualization-studio.html">Where is the location of the custom Javascript visualization code created using Chart Studio?</a>
                                </li>
                                <li><a href="../what-is-alexa-echo-integration.html">What is Amazon Echo/Alexa integration?</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </body>
</html>